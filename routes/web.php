<?php
use Illuminate\Http\Response;
/** @var \Laravel\Lumen\Routing\Router $router */

function generate_string($strength = 16) {
    $input = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $input_length = strlen($input);
    $random_string = '';
    for($i = 0; $i < $strength; $i++) {
        $random_character = $input[mt_rand(0, $input_length - 1)];
        $random_string .= $random_character;
    }
 
    return $random_string;
}


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Hi there<br>This app is in development, when there is a beta version available you\'ll find the link to the app here.';
});

/*$router->get('/dbtest/{id}', function($id) use ($router) {
    $entries = DB::select('SELECT id FROM users WHERE tg_id = :id', ['id' => $id]);

    return $entries == null;
});*/

$router->get('/get/payload', function() {
    $gen_payload = generate_string(20);
    DB::insert('INSERT INTO payloads (payload) values (:payload)', ['payload' => $gen_payload]);
    return $gen_payload;
});
$router->get('/get/token/{payload}', function($payload) {
    $token = "undefined";
    $tokens = DB::select('SELECT token FROM payloads WHERE payload = :payload', ['payload' => $payload]);
    if($tokens != null) {
        $testtoken = $tokens[0]->token;
        if(strlen($testtoken) == 20) $token = $testtoken;
    }
    return $token;
});
$router->get('/get/user/{token}', function($token) {
    $ids = DB::select('SELECT users_id FROM loggedIn WHERE token = :token', ['token' => $token]);
    if($ids == null) return "";
    $userid = $ids[0]->users_id;

    $users = DB::select('SELECT id, prename, lastname, pb, emoji FROM users WHERE id = :userid', ['userid' => $userid]);
    if($users == null) return "";
    $user = $users[0]->prename;
    $user .= " ";
    $user .= $users[0]->lastname;
    return response()->json($users[0], 200, []);; 
});
$router->get('/get/picture/{token}', function($token) {
    $ids = DB::select('SELECT tg_id, id FROM users WHERE id = (SELECT users_id FROM loggedIn WHERE token = :token)', ['token' => $token]);
    if($ids == null) return "";
    $tgid = $ids[0]->tg_id;
    $id = $ids[0]->id;
    $envtoken = env('TELEGRAM_TOKEN',false);
    $url = "https://api.telegram.org";
    $bot = "bot$envtoken";
    $pics = file("$url/$bot/getUserProfilePhotos?user_id=$tgid");
    $json = json_decode($pics[0]);
    $file_id = $json->result->photos["0"]["1"]->file_id;
    $filepathres = file("$url/$bot/getFile?file_id=$file_id");
    $json = json_decode($filepathres[0]);
    $filepath = $json->result->file_path;
    $photo = file("$url/file/$bot/$filepath");
    $ch = curl_init("$url/file/$bot/$filepath");
    $folder = $_SERVER['DOCUMENT_ROOT'];
    $fp = fopen("$folder/profile_pics/$id.jpg", 'w');
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
    return true;
});

// HANDLE TELEGRAM WEBHOOKS
$router->post('/tg/{token}', function($token) {
    // return "";
    // Prepare token and request domain
    $tgdomain = getTgDomain($token);
    if($tgdomain == "") return "";

    // Read from webhook
    $tgreq = file_get_contents('php://input');
    $json = json_decode($tgreq, true);

    // Read  message
    $msg_text = $json["message"]["text"];
    $from = $json["message"]["from"];
    $id = $from["id"];

    // Message contains start, go to login function
    if(strpos($msg_text, "/start") !== false){
        if(key_exists("first_name", $from))
            $prename = $from["first_name"];
        else $prename = "";
        if(key_exists("last_name", $from))
            $lastname = $from["last_name"];
        else $lastname = "";
        $id = $from["id"];

        if($prename == null || strlen($prename) < 1) $prename = "";
        if($lastname == null || strlen($lastname) < 1) $lastname = "";
        // Read and check payload
        $payload = trim(str_replace("/start", "", $msg_text));
        //$payloads = DB::select('SELECT id FROM payloads WHERE payload = :payload', ['payload' => $payload]);
        if(strlen($payload) != 20){
            $answer = "Bitte Loginanfrage von der App her senden ;)";
            $res = file("$tgdomain/sendMessage?chat_id=$id&text=$answer");
            return "false";
        }

        // Get user id if user exists
        $entries = DB::select('SELECT id, prename, lastname, emoji FROM users WHERE tg_id = :id', ['id' => $id]);
            // Create user if doesnt exist
        if($entries == null){
            DB::insert('INSERT INTO users (prename, lastname, tg_id) values (:pname, :lname, :tgid)', ['pname' => $prename, 'lname' => $lastname, 'tgid' => $id]);
            $entries = DB::select('SELECT id, lastname, emoji FROM users WHERE tg_id = :id', ['id' => $id]);
            handleIncompleteProfile($id, $payload, $entries[0]);
        } else {
            if(handleIncompleteProfile($id, $payload, $entries[0])){
                doLogin($id, $payload, $entries[0]);
            }
        }
    } elseif(strpos($msg_text, "/new_name") !== false) {
        $id = $from["id"];
        // DB::update('UPDATE users set prename = "", lastname = "" where tg_id = :id', ['id' => $id]);
        DB::update('UPDATE conversations SET cstate = "name", payload = "" WHERE tg_id = :id', ['id' => $id]);
        $r = file("$tgdomain/sendMessage?chat_id=$id&text=Sende deinen Namen bitte im Format: \"Vorname Nachname\" (ohne Anführungszeichen)");
    } elseif(strpos($msg_text, "/logout_everywhere") !== false) {
        $id = $from["id"];
        DB::delete('DELETE FROM loggedIn WHERE users_id = (SELECT id FROM users WHERE tg_id = :id LIMIT 1)', ['id' => $id]);
        $r = file("$tgdomain/sendMessage?chat_id=$id&text=Du bist nun überall ausgeloggt.");
    } elseif(strpos($msg_text, "/new_emoji") !== false) {
        $id = $from["id"];
        DB::update('UPDATE conversations SET cstate = "emoji", payload = "" WHERE tg_id = :id', ['id' => $id]);
        $r = file("$tgdomain/sendMessage?chat_id=$id&text=Du darfst nun ein neues Emoji auswählen ;)");
    }else {
        $entries = DB::select('SELECT prename, lastname, tg_id, id, emoji from users where tg_id = :tgid', ["tgid" => $id]);
        $entry = $entries[0];
        $pname = $entry->prename;
        $lname = $entry->lastname;
        $conversation = getConversation($id);
        if($conversation->cstate == "emoji")
        {
            $emojiRegexp = '([*#0-9](?>\\xEF\\xB8\\x8F)?\\xE2\\x83\\xA3|\\xC2[\\xA9\\xAE]|\\xE2..(\\xF0\\x9F\\x8F[\\xBB-\\xBF])?(?>\\xEF\\xB8\\x8F)?|\\xE3(?>\\x80[\\xB0\\xBD]|\\x8A[\\x97\\x99])(?>\\xEF\\xB8\\x8F)?|\\xF0\\x9F(?>[\\x80-\\x86].(?>\\xEF\\xB8\\x8F)?|\\x87.\\xF0\\x9F\\x87.|..(\\xF0\\x9F\\x8F[\\xBB-\\xBF])?|(((?<zwj>\\xE2\\x80\\x8D)\\xE2\\x9D\\xA4\\xEF\\xB8\\x8F\k<zwj>\\xF0\\x9F..(\k<zwj>\\xF0\\x9F\\x91.)?|(\\xE2\\x80\\x8D\\xF0\\x9F\\x91.){2,3}))?))';
            // $emojiRegexp = '/[\p{N}]/';
            if(preg_match($emojiRegexp, $msg_text) && ((mb_strlen($msg_text, 'UTF-8') >= 1 && mb_strlen($msg_text, 'UTF-8') <= 20))){
                $emoji = mb_substr($msg_text,0,1);
                // $r = file("$tgdomain/sendMessage?chat_id=$id&text=emoji $emoji");
                $responses = DB::select('SELECT id FROM users WHERE prename = :prename AND emoji = :emoji', ['prename' => $pname, 'emoji' => $emoji]);
                if($responses != null){
                    $r = file("$tgdomain/sendMessage?chat_id=$id&text=Es benutzt bereits jemand \"$pname $emoji\". Schick ein anderes Emoji ;)");
                    return "";
                }
                DB::update('UPDATE users SET emoji = :emoji WHERE tg_id = :id', ['emoji' => $emoji, 'id' => $id]);
                $r = file("$tgdomain/sendMessage?chat_id=$id&text=✅ Wir nennen dich nun \"$pname $msg_text\"");
                $payload = $conversation->payload;
                DB::update('UPDATE conversations SET cstate = "", payload = "" WHERE tg_id = :id', ['id' => $id]);

                if($payload != ""){
                    doLogin($id, $payload, $entry);
                }
            } else {
                $r = file("$tgdomain/sendMessage?chat_id=$id&text=Bitte sende ein echtes Emoji 🙃");
            }
        } else if ($conversation->cstate == "name") {
            $splitted = explode(" ", $msg_text);
            if(count($splitted) == 2){
                $prename = $splitted[0];

                $lastname = $splitted[1];
                DB::update('UPDATE users set prename = :pname, lastname = :lname where tg_id = :tgid', ['pname' => $prename, 'lname' => $lastname, 'tgid' => $id]);
                DB::update('UPDATE conversations SET cstate = "", payload = "" WHERE tg_id = :id', ['id' => $id]);
                $r = file("$tgdomain/sendMessage?chat_id=$id&text=Du heisst jetzt \"$prename $lastname\"");
            } else {
                $r = file("$tgdomain/sendMessage?chat_id=$id&text=Sende deinen Namen bitte im Format: \"Vorname Nachname\" (ohne Anführungszeichen)");
            }
        } else if ($conversation->cstate == "lastname") {
            if(strlen($msg_text) < 3) {
                $r = file("$tgdomain/sendMessage?chat_id=$id&text=Dieser Name ist etwas kurz ;) Bitte einen längeren senden");
                return "";
            }
            DB::update('UPDATE users set lastname = :lname where tg_id = :tgid', ['lname' => $msg_text, 'tgid' => $id]);
            DB::update('UPDATE conversations SET cstate = "", payload = "" WHERE tg_id = :id', ['id' => $id]);
            $r = file("$tgdomain/sendMessage?chat_id=$id&text=Du heisst jetzt \"$pname $msg_text\"");
        } else {
            $r = file("$tgdomain/sendMessage?chat_id=$id&text=Dieser Befehl ist unbekannt.");
        }
    }
    return "";
});

function handleIncompleteProfile($id, $payload, $entry) {
    $tgdomain = getTgDomain();
    $lastname = $entry->lastname;
    if($lastname == "") {
        $r = file("$tgdomain/sendMessage?chat_id=$id&text=Bitte sende noch deinen Nachnamen, um dein Profil zu vervollständigen :)");
        $cs = getConversation($id);
        DB::update('UPDATE conversations SET cstate = "lastname", payload = :payload WHERE tg_id = :id', ['payload' => $payload, 'id' => $id]);
        return false;
    }
    if($entry->emoji == ""){
        $r = file("$tgdomain/sendMessage?chat_id=$id&text=Sende nun ein cooles Emoji, welches dann mit deinem Vornamen zum Benutzernamen wird (zb. Fritz 🥳)");
        $cs = getConversation($id);
        DB::update('UPDATE conversations SET cstate = "emoji", payload = :payload WHERE tg_id = :id', ['payload' => $payload, 'id' => $id]);
        return false;
    }
    return true;
}

function getTgDomain($token = null){
    $envtoken = env('TELEGRAM_TOKEN',false);
    if($token != null && $envtoken != $token) return "";
    return "https://api.telegram.org/bot$envtoken";
}

function getConversation($id){
    $entries = DB::select('SELECT tg_id, cstate, payload FROM conversations WHERE tg_id = :id', ['id' => $id]);
    if($entries == null) {
        DB::insert("INSERT INTO conversations (tg_id) VALUES (:id)", ['id' => $id]);
        $entries = DB::select('SELECT tg_id, cstate, payload FROM conversations WHERE tg_id = :id', ['id' => $id]);
    }
    return $entries[0];
}

function doLogin($id, $payload, $entry){
    $tgdomain = getTgDomain();
    $dbid = $entry->id;
    $prename = $entry->prename;
    // Create and save token
    $token = generate_string(20);
    DB::insert('INSERT INTO loggedIn (token, users_id) values (:token, :userid)', ['token' => $token, 'userid' => $dbid]);
    DB::update('INSERT INTO payloads (token, payload) values (:token, :payload)', ['token' => $token, 'payload' => $payload]);

    // Confirm login to user
    $answer = urlencode("Hallo $prename, du hast dich erfolgreich eingeloggt!✅\n\n Zurück zur App:\n➡️ prayfor.nasroni.one");
    $res = file("$tgdomain/sendMessage?chat_id=$id&text=$answer&parse_mode=markdown");
}

// Explain android that the app is mine :)
$router->get('/.well-known/assetlinks.json', function(){
    return '[{
  "relation": ["delegate_permission/common.handle_all_urls"],
  "target": {
    "namespace": "android_app",
    "package_name": "one.nasroni.prayfor",
    "sha256_cert_fingerprints":
    ["66:95:F2:37:00:22:77:0C:F0:F6:9F:A7:2E:DB:18:E5:60:1E:27:43:5A:AF:0D:59:51:3A:72:66:8B:CA:84:C6"]
  }
}]
';
});